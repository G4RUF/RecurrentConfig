###############################################################################
#
# File Name         : install.sh
# Created By        : Guillaume FAVRE
# Creation Date     : juin 23th, 2015
# Version           : 0.1
# Last Change      : novembre  2th, 2016 at 10:57:34
# Last Changed By  : Guillaume FAVRE
# Purpose           : Description
#
###############################################################################
#!/usr/bin/bash
# Repository to install vlc
dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
#Function to download different files
if [ $(id -u) = 0 ]; then
	listPrograms='vim terminator vlc mozilla-vlc nmap python2 python-dev hping scapy'
	for program in $listPrograms; do
		echo "Install " $program   
		dnf install -y $program
	done
	bash init.sh
else
	echo "Root mode necessary"
fi
